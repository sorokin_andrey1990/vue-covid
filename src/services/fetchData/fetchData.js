import { formatedDate } from "@/services/fetchData/logic/summary";

export class getData {
	constructor(id) {
		this.url = "https://api.covid19api.com";
		this.id = id;
	}

	async fetchSummary() {
		const res = await fetch(this.url + "/summary");
		const data = await res.json();
		const changedDate = formatedDate(data.Date);
		return {
			dataDate: changedDate,
			stats: data.Global,
			countries: data.Countries,
		};
	}

	async fetchCountries() {
		const res = await fetch(this.url + "/countries");
		const data = await res.json();
		return data;
	}
}
