import moment from "moment";

export const formatedDate = (dataDate, id = "MMMM Do YYYY, h:mm:ss a") => {
	return moment(dataDate).format(id);
};
