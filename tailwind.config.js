module.exports = {
	purge: { content: ["./public/**/*.html", "./src/**/*.vue"] },
	darkMode: false, // or 'media' or 'class'
	theme: {
		extended: {},
	},
	variants: {
		extend: {},
	},
	plugins: [],
};
